package de.rwth.swc.sqa.managers;

import java.util.HashMap;
import java.util.Random;

import de.rwth.swc.sqa.model.Customer;

public class CustomerManager {
    public static HashMap<Long, Customer> customers = new HashMap<>();

    public static Customer getCustomer(Long id) {
        return customers.get(id);
    }

    public static void addCustomer(Customer customer) {
        customers.put(customer.getId(), customer);
    }

    public static void removeCustomer(Long id) {
        customers.remove(id);
    }

    public static void clearCustomers() {
        customers.clear();
    }

    public static boolean containsCustomer(Long id) {
        return customers.containsKey(id);
    }

    public static Long generateCustomerId(){
        Random r = new Random();
        do{
            Long id = Math.abs(r.nextLong());
            if(!containsCustomer(id)){
                return id;
            }
        }while(true);
    }


}
