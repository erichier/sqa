package de.rwth.swc.sqa.managers;

import java.util.HashMap;
import java.util.Random;

import de.rwth.swc.sqa.model.Ticket;

public class TicketManager {
    public static HashMap<Long, Ticket> tickets = new HashMap<>();
    
    public static Ticket getTicketById(Long id) {
        return tickets.get(id);
    }

    public static void addTicketById(Ticket ticket) {
        tickets.put(ticket.getId(), ticket);
    }

    public static void removeTicketById(Long id) {
        tickets.remove(id);
    }

    public static void clearTicketsById() {
        tickets.clear();
    }

    public static boolean containsTicketById(Long id) {
        return tickets.containsKey(id);
    }

    public static Long generateTicketId() {
        Random r = new Random();
        do{
            Long id = Math.abs(r.nextLong());
            if(!containsTicketById(id)){
                return id;
            }
        }while(true);
    }


}
