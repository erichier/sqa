package de.rwth.swc.sqa.managers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import de.rwth.swc.sqa.model.DiscountCard;

public class DiscountCardManager {

    public static HashMap<Long, DiscountCard> discountCardsById = new HashMap<>();
    public static HashMap<Long, ArrayList<DiscountCard>> discountCardsByCustomerId = new HashMap<>();

    public static DiscountCard getDiscountCardById(Long id) {
        return discountCardsById.get(id);
    }

    public static ArrayList<DiscountCard> getDiscountCardsByCustomerId(Long id) {
        if(discountCardsByCustomerId.isEmpty()) return new ArrayList<DiscountCard>();
        return discountCardsByCustomerId.get(id);
    }

    public static void addDiscountCard(DiscountCard discountCard) {
        discountCardsById.put(discountCard.getId(), discountCard);
        if(discountCardsByCustomerId.containsKey(discountCard.getCustomerId())) {
            discountCardsByCustomerId.get(discountCard.getCustomerId()).add(discountCard);
        } else {
            ArrayList<DiscountCard> discountCards = new ArrayList<>();
            discountCards.add(discountCard);
            discountCardsByCustomerId.put(discountCard.getCustomerId(), discountCards);
        }
    }

    public static void removeDiscountCardById(Long id) {
        discountCardsByCustomerId.get(getDiscountCardById(id).getCustomerId()).remove(getDiscountCardById(id));
        discountCardsById.remove(id);
    }

    public static void clearDiscountCardsById() {
        discountCardsById.clear();
        discountCardsByCustomerId.clear();
        
    }

    public static boolean containsDiscountCardById(Long id) {
        return discountCardsById.containsKey(id);
    }

    public static boolean containsDiscountCardByCustomerId(Long id) {
        return discountCardsByCustomerId.get(id) != null || !discountCardsByCustomerId.get(id).isEmpty();
    }
    
    public static void ClearDiscountCards(){
        discountCardsById.clear();
        discountCardsByCustomerId.clear();
    }

    public static Long generateDiscountCardId() {
        Random r = new Random();
        do {
            Long id = Math.abs(r.nextLong());
            if(!containsDiscountCardById(id)){
                return id;
            }
        } while(true);
    }
    
}
