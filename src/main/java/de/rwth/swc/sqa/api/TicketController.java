package de.rwth.swc.sqa.api;

import de.rwth.swc.sqa.logic.TicketUtilities;
import de.rwth.swc.sqa.logic.TicketUtilities.TypeEnum;
import de.rwth.swc.sqa.managers.TicketManager;
import de.rwth.swc.sqa.model.Ticket;
import de.rwth.swc.sqa.model.TicketRequest;
import de.rwth.swc.sqa.model.TicketValidationRequest;
import io.swagger.annotations.ApiParam;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.Date;

import de.rwth.swc.sqa.logic.CommonUtilities;

@Controller
public class TicketController implements TicketsApi{
    

    public ResponseEntity<Void> validateTicket(@ApiParam(value = "TicketValidationRequest object that needs to validated" ,required=true )  @Valid @RequestBody TicketValidationRequest body) {
        Ticket ticket;
        if(TicketManager.containsTicketById(body.getTicketId())){
            ticket = TicketManager.getTicketById(body.getTicketId());
        }
        else {
            return new ResponseEntity<Void>(HttpStatus.FORBIDDEN);
        }

        int hoursValid = TicketUtilities.hoursTicketIsValid(ticket);
        Date validFrom = CommonUtilities.parseDateTime(ticket.getValidFrom());
        Date upperDateLimit;
        Date lowerDateLimit;

        if(validFrom != null){
            lowerDateLimit = validFrom;
            upperDateLimit = CommonUtilities.addHoursToJavaUtilDate(validFrom, hoursValid);
        }
        else {
            return new ResponseEntity<Void>(HttpStatus.FORBIDDEN);
        }

        Date ticketDate = CommonUtilities.parseDateTime(body.getDate());

        if(ticketDate == null ||  !(ticketDate.after(lowerDateLimit) && ticketDate.before(upperDateLimit))){
            return new ResponseEntity<Void>(HttpStatus.FORBIDDEN);
        }

        if(!TicketUtilities.validZone(ticket.getZone(), body.getZone())){
            return new ResponseEntity<Void>(HttpStatus.FORBIDDEN);
        }

        if(body.getStudent() != null && (ticket.getStudent() && !body.getStudent())){
            return new ResponseEntity<Void>(HttpStatus.FORBIDDEN);
        }

        if(body.getDisabled() != null && (ticket.getDisabled() != body.getDisabled())){
            return new ResponseEntity<Void>(HttpStatus.FORBIDDEN);
        }

        if(body.getDiscountCardId() != null && !ticket.getDiscountCard()){
            return new ResponseEntity<Void>(HttpStatus.FORBIDDEN);
        }

        return new ResponseEntity<>(HttpStatus.OK);

    }

    public ResponseEntity<Ticket> buyTicket(@ApiParam(value = "TicketRequest object" ,required=true )  @Valid @RequestBody TicketRequest body) {
        TypeEnum type = TypeEnum.Adult;
        Ticket newticket = new Ticket();
        newticket.id(TicketManager.generateTicketId());
        Date validFrom = CommonUtilities.parseDateTime(body.getValidFrom());
        if(validFrom != null){
            newticket.validFrom(body.getValidFrom());
        }   
        else {    
            return new ResponseEntity<Ticket>(HttpStatus.FORBIDDEN);
        }

        Date birthdDate = CommonUtilities.parseDateTime(body.getBirthdate());
        if(birthdDate != null){
            newticket.birthdate(body.getBirthdate());
        }
        else {   
            return new ResponseEntity<Ticket>(HttpStatus.FORBIDDEN);
        }

        if(body.getValidFor() != null){
            newticket.validFor(TicketUtilities.convertValidForEnum(body.getValidFor()));
        }
        else {
            return new ResponseEntity<Ticket>(HttpStatus.FORBIDDEN);
        }

        if(body.getZone() != null){
            newticket.zone(TicketUtilities.convertZoneEnum(body.getZone()));
        }
        else {
            return new ResponseEntity<Ticket>(HttpStatus.FORBIDDEN);
        }

        if(body.getDisabled() != null){
            newticket.disabled(body.getDisabled());
        }
        else {
            newticket.disabled(false);
        }

        if(body.getDiscountCard() != null){
            newticket.discountCard(body.getDiscountCard());
        }
        else {
            newticket.discountCard(false);
        }

        if(body.getStudent() != null){
            newticket.student(body.getStudent());
        }
        else {
            newticket.student(false);
        }

        int age = CommonUtilities.getDiffYears(birthdDate, validFrom);
        if(age >= 60 ){
            type = TypeEnum.Senior;
        }

        else if (age < 14){
            type = TypeEnum.Child;  
        }

        else if (age < 28 && newticket.getStudent() == true){
            type = TypeEnum.Student;
        }

        else {
            type = TypeEnum.Adult;
        }

        if(type == TypeEnum.Senior && newticket.getValidFor() == Ticket.ValidForEnum._1H){
            return new ResponseEntity<Ticket>(HttpStatus.FORBIDDEN);
        }

        else if(type == TypeEnum.Child && newticket.getValidFor() == Ticket.ValidForEnum._1D){
            return new ResponseEntity<Ticket>(HttpStatus.FORBIDDEN);
        }

        else if(type == TypeEnum.Student && (newticket.getValidFor() == Ticket.ValidForEnum._1H || newticket.getValidFor() == Ticket.ValidForEnum._1D)){
            return new ResponseEntity<Ticket>(HttpStatus.FORBIDDEN);
        }

        TicketManager.addTicketById(newticket);
        return new ResponseEntity<Ticket>(newticket ,HttpStatus.CREATED);
    }

}
