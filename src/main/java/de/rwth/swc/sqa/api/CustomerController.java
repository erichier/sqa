package de.rwth.swc.sqa.api;

import de.rwth.swc.sqa.managers.DiscountCardManager;
import org.springframework.stereotype.Controller;

import de.rwth.swc.sqa.logic.CommonUtilities;
import de.rwth.swc.sqa.logic.TicketUtilities;
import de.rwth.swc.sqa.managers.CustomerManager;
import de.rwth.swc.sqa.model.Customer;
import de.rwth.swc.sqa.model.DiscountCard;
import io.swagger.annotations.*;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class CustomerController implements CustomersApi {

    private static int hoursDiscountCardIsValid (DiscountCard discountCard) {
        int hoursValid = 0;
        if (discountCard.getValidFor() == DiscountCard.ValidForEnum._30D) {
            hoursValid=24*30;
        }
        else if (discountCard.getValidFor() == DiscountCard.ValidForEnum._1Y) {
            hoursValid=24*30*365;
        }
        return hoursValid;
    }

    public ResponseEntity<Customer> addCustomer(@ApiParam(value = "Customer object that should be added" ,required=true )  @Valid @RequestBody Customer body) {

        Customer customer = new Customer();

        customer.setId(CustomerManager.generateCustomerId());
        customer.setBirthdate(body.getBirthdate());


        if(body.getDisabled() != null){
            customer.setDisabled(body.getDisabled());
        }
        else {
            customer.setDisabled(false);
        }

        CustomerManager.addCustomer(customer);


        return new ResponseEntity<Customer>(customer, HttpStatus.OK);
    }

    public ResponseEntity<DiscountCard> addDiscountCardToCustomer(Long customerId, DiscountCard body) {
        // check if customer exists
        if (!CustomerManager.containsCustomer(customerId))
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        // todo: check if there is already a discount card in that timeframe
        Date startDate = CommonUtilities.parseDateTime(body.getValidFrom());
        Date endDate = CommonUtilities.addHoursToJavaUtilDate(startDate,  hoursDiscountCardIsValid(body));

        for (DiscountCard card : DiscountCardManager.getDiscountCardsByCustomerId(customerId)) {
            Date conflictStartDate = CommonUtilities.parseDateTime(card.getValidFrom());
            Date conflictEndDate= CommonUtilities.addHoursToJavaUtilDate(conflictStartDate,  hoursDiscountCardIsValid(body));
            
            if((startDate.compareTo(conflictEndDate) <= 0  && startDate.compareTo(conflictStartDate) >= 0)
                || (endDate.compareTo(conflictStartDate)>=0 && endDate.compareTo(conflictEndDate)<=0)){
                return new ResponseEntity<>(HttpStatus.CONFLICT);
            }
        }

        body.setCustomerId(customerId);
        body.setId(DiscountCardManager.generateDiscountCardId());

        DiscountCardManager.addDiscountCard(body);

        return new ResponseEntity<DiscountCard>(body, HttpStatus.CREATED);
    }

    public ResponseEntity<List<DiscountCard>> getCustomerDiscountCards(Long customerId) {
        if (!CustomerManager.containsCustomer(customerId))
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        ArrayList<DiscountCard> list = DiscountCardManager.getDiscountCardsByCustomerId(customerId);

        if (list.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<List<DiscountCard>>(list, HttpStatus.OK);
    }

}
