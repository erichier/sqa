package de.rwth.swc.sqa.logic;

import de.rwth.swc.sqa.model.Ticket;
import de.rwth.swc.sqa.model.TicketRequest;
import de.rwth.swc.sqa.model.TicketValidationRequest;

public class TicketUtilities {

    public static enum TypeEnum {
        Senior, Adult, Student, Child;
    }

    public static int hoursTicketIsValid (Ticket ticket) {
        int hoursValid = 0;
        if (ticket.getValidFor() == Ticket.ValidForEnum._1H) {
            hoursValid=1;
        }
        else if (ticket.getValidFor() == Ticket.ValidForEnum._1D) {
            hoursValid=24;
        }
        else if (ticket.getValidFor() == Ticket.ValidForEnum._30D) {
            hoursValid=24*30;
        }
        else if (ticket.getValidFor() == Ticket.ValidForEnum._1Y) {
            hoursValid=24*30*365;
        }
        return hoursValid;
    }

    public static boolean validZone(Ticket.ZoneEnum ticketZone, TicketValidationRequest.ZoneEnum actualZone) {    
        boolean valid = false;
        if(ticketZone == Ticket.ZoneEnum.A && actualZone == TicketValidationRequest.ZoneEnum.A) {
            valid = true;
        }
        else if(ticketZone == Ticket.ZoneEnum.B && (actualZone == TicketValidationRequest.ZoneEnum.A || actualZone == TicketValidationRequest.ZoneEnum.B)) {
            valid = true;
        }
        else  if(ticketZone == Ticket.ZoneEnum.C && (actualZone == TicketValidationRequest.ZoneEnum.A || actualZone == TicketValidationRequest.ZoneEnum.B || actualZone == TicketValidationRequest.ZoneEnum.C)) {
            valid = true;
        }
        return valid;
    }
    
    public static Ticket.ValidForEnum convertValidForEnum(TicketRequest.ValidForEnum requestEnum){
        Ticket.ValidForEnum ticketEnum = Ticket.ValidForEnum._1H;
        if(requestEnum == TicketRequest.ValidForEnum._1H){
            ticketEnum = Ticket.ValidForEnum._1H;
        }
        else if(requestEnum == TicketRequest.ValidForEnum._1D){
            ticketEnum = Ticket.ValidForEnum._1D;
        }
        else if(requestEnum == TicketRequest.ValidForEnum._30D){
            ticketEnum = Ticket.ValidForEnum._30D;
        }
        else if(requestEnum == TicketRequest.ValidForEnum._1Y){
            ticketEnum = Ticket.ValidForEnum._1Y;
        }
        return ticketEnum;
    }
    
    public static Ticket.ZoneEnum convertZoneEnum(TicketRequest.ZoneEnum requestEnum){
        Ticket.ZoneEnum zoneEnum = Ticket.ZoneEnum.A;
        if(requestEnum == TicketRequest.ZoneEnum.A){
            zoneEnum = Ticket.ZoneEnum.A;
        }
        else if(requestEnum == TicketRequest.ZoneEnum.B){
            zoneEnum = Ticket.ZoneEnum.B;
        }
        else if(requestEnum == TicketRequest.ZoneEnum.C){
            zoneEnum = Ticket.ZoneEnum.C;
        }
        return zoneEnum;
    }

}
