package de.rwth.swc.sqa;


import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.hamcrest.Matchers;

import de.rwth.swc.sqa.model.Customer;

import static io.restassured.RestAssured.given;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
public class CustomerCreationTest {

    private static final String ADD_CUSTOMER_PATH = "/customers";
    private static final Customer customer = new Customer();

    @LocalServerPort
    private int port;

    @BeforeEach
    public void setUp() {
        RestAssured.port = port;

        customer.setId(123456L);
        customer.setBirthdate("1998-06-19");
        customer.setDisabled(false);
    }

    @Test
    public void customerCreation() {
        given().contentType("application/json").body(customer).when().post(ADD_CUSTOMER_PATH)
        .then()
        .statusCode(200)
        .body("birthdate",Matchers.equalTo("1998-06-19"))
        .body("disabled", Matchers.equalTo(false));
    }
}
