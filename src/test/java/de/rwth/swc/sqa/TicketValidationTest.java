package de.rwth.swc.sqa;

import de.rwth.swc.sqa.api.TicketController;
import de.rwth.swc.sqa.managers.TicketManager;
import de.rwth.swc.sqa.model.Ticket;
import de.rwth.swc.sqa.model.TicketRequest;
import de.rwth.swc.sqa.model.TicketValidationRequest;
import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import static io.restassured.RestAssured.given;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
public class TicketValidationTest {

    private static final String TICKET_VALIDATION_PATH = "/tickets/validate";
    private static final String TICKET_REQUEST_PATH = "/tickets";
    private static final Ticket ticket = new Ticket();
    private static final TicketValidationRequest request = new TicketValidationRequest();

    @LocalServerPort
    private int port;

    @BeforeEach
    public void setUp() {
        RestAssured.port = port;

        // prepare a ticket
        ticket.setBirthdate("2001-02-19");
        ticket.setDisabled(false);
        ticket.setStudent(false);
        ticket.setValidFrom("2022-05-23T10:38:59");
        ticket.setZone(Ticket.ZoneEnum.A);
        ticket.setValidFor(Ticket.ValidForEnum._30D);

        // with a valid ticket request
        request.setZone(TicketValidationRequest.ZoneEnum.A);
        request.setDate("2022-05-23T12:38:59");
        request.setDisabled(false);
        request.setDiscountCardId(null);
        request.setStudent(false);

        updateTicketId();
    }

    private void updateTicketId() {
        long id = given().contentType("application/json").body(ticket).when().post(TICKET_REQUEST_PATH).getBody().as(Ticket.class).getId();
        ticket.setId(id);
        request.setTicketId(id);
    }

    @Test
    public void defaultTicket() {
        given().contentType("application/json").body(request).when().post(TICKET_VALIDATION_PATH).then().statusCode(200);
    }

    @Test
    public void outOfDateTicket() {
        request.setDate("2024-02-19T12:38:59");
        updateTicketId();
        given().contentType("application/json").body(request).when().post(TICKET_VALIDATION_PATH).then().statusCode(403);
    }

    @Test
    public void studentUsingAdultTicket() {
        request.setStudent(true);
        updateTicketId();
        given().contentType("application/json").body(request).when().post(TICKET_VALIDATION_PATH).then().statusCode(200);
    }

    @Test
    public void bookedNotEnoughZones() {
        request.setZone(TicketValidationRequest.ZoneEnum.B);
        updateTicketId();
        given().contentType("application/json").body(request).when().post(TICKET_VALIDATION_PATH).then().statusCode(403);
    }

    @Test
    public void ticketExpired() {
        // ticket date: 2022-05-23T10:38:59
        // valid the day before
        request.setDate("2022-06-22T10:38:58");
        updateTicketId();
        given().contentType("application/json").body(request).when().post(TICKET_VALIDATION_PATH).then().statusCode(200);

        // expired the day after
        request.setDate("2022-06-22T10:38:59");
        updateTicketId();
        given().contentType("application/json").body(request).when().post(TICKET_VALIDATION_PATH).then().statusCode(403);
    }
}
