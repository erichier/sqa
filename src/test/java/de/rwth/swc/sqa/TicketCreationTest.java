package de.rwth.swc.sqa;

import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.hamcrest.Matchers;

import de.rwth.swc.sqa.model.Ticket;

import static io.restassured.RestAssured.given;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
public class TicketCreationTest {

    private static final String ADD_TICKET_PATH = "/tickets";
    private static final Ticket ticket = new Ticket();

    @LocalServerPort
    private int port;

    @BeforeEach
    public void setUp() {
        RestAssured.port = port;

        ticket.setId(123456789L); 
        ticket.setBirthdate("1998-06-19");
        ticket.setValidFrom("2022-05-23T10:38:59");
        ticket.setValidFor(Ticket.ValidForEnum._30D);
        ticket.setDisabled(false);
        ticket.setDiscountCard(true);
        ticket.setZone(Ticket.ZoneEnum.A);
        ticket.setDisabled(false);
        ticket.setStudent(false);

    }

    @Test
    public void ticketCreation() {
        given().contentType("application/json").body(ticket).when().post(ADD_TICKET_PATH)
        .then().statusCode(201)
        .body("birthdate", Matchers.equalTo("1998-06-19"))
        .body("validFrom", Matchers.equalTo("2022-05-23T10:38:59"))
        .body("validFor", org.hamcrest.Matchers.equalTo(ticket.getValidFor().toString()))
        .body("discountCard", Matchers.equalTo(true))
        .body("zone", Matchers.equalTo(Ticket.ZoneEnum.A.toString()))
        .body("disabled", Matchers.equalTo(false))
        .body("student", Matchers.equalTo(false));
    }

}
